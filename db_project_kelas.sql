-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 15 Jun 2023 pada 05.29
-- Versi server: 10.4.24-MariaDB
-- Versi PHP: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_project_kelas`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang`
--

CREATE TABLE `barang` (
  `kodeBarang` char(12) NOT NULL,
  `namaBarang` varchar(100) NOT NULL,
  `jumlahBarang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `barang`
--

INSERT INTO `barang` (`kodeBarang`, `namaBarang`, `jumlahBarang`) VALUES
('', '', 0),
('001', 'Komputer', 50),
('002', 'Laptop', 100),
('004', 'Printer', 111),
('tes1', 'tes1', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `mhsNpm` char(12) NOT NULL,
  `mhsNama` varchar(255) DEFAULT NULL,
  `mhsAlamat` text DEFAULT NULL,
  `mhsFakultas` varchar(255) DEFAULT NULL,
  `mhsProdi` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `mahasiswa`
--

INSERT INTO `mahasiswa` (`mhsNpm`, `mhsNama`, `mhsAlamat`, `mhsFakultas`, `mhsProdi`) VALUES
('11312136', 'Mahasiswa1', 'alamat mahasiswa 1\r\n', 'Teknik', 'Teknik Informatika'),
('11312137', 'Mahasiswa2', 'alamat mahasiswa 2\r\n', 'Teknik', 'Teknik Elektro'),
('11312138', 'Mahasiswa 3', 'alamat mahasiswa 3', 'fakultas mahasiswa 3', 'prodi mahasiswa 3');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengajuan`
--

CREATE TABLE `pengajuan` (
  `kodePengajuan` char(12) NOT NULL,
  `tglPengajuan` date NOT NULL,
  `npmPeminjam` char(12) NOT NULL,
  `namaPeminjam` varchar(100) NOT NULL,
  `mhsProdi` varchar(100) NOT NULL,
  `noHandphone` char(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pengajuan`
--

INSERT INTO `pengajuan` (`kodePengajuan`, `tglPengajuan`, `npmPeminjam`, `namaPeminjam`, `mhsProdi`, `noHandphone`) VALUES
('001', '2023-06-01', '19311021', 'Chairul Fahri', 'Sistem Informasi', '082280808080'),
('002', '2023-01-01', '19311024', 'Wisnu Mukti', 'sistem informasi', '089929299');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `name`, `username`, `pass`, `role`) VALUES
(1, 'admin', 'tes', '28b662d883b6d76fd96e4ddc5e9ba780', 'Admin'),
(2, 'admin2', 'Admin2', '7a8a80e50f6ff558f552079cefe2715d', 'Admin'),
(4, 'tess1', 'tes11', '22daf1a39b6e5ea16554f59e472d96f6', 'Admin'),
(6, 'admin1', 'admin1', '0192023a7bbd73250516f069df18b500', 'Admin');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`kodeBarang`);

--
-- Indeks untuk tabel `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`mhsNpm`);

--
-- Indeks untuk tabel `pengajuan`
--
ALTER TABLE `pengajuan`
  ADD PRIMARY KEY (`kodePengajuan`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
